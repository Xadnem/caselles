var express = require('express');
const DAOMongo = require('../public/javascripts/DAOMongo');
var router = express.Router();

/* GET partides listing. */
router.get('/', function(req, res, next) {
  DAOMongo.mostrarPartides(res);
});

module.exports = router;
