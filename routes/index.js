var express = require('express');
var router = express.Router();
const DAOMongo = require('../public/javascripts/DAOMongo');






/* GET home page. */
router.get('/', function(req, res, next) {

  DAOMongo.mostrarJugadors(res);
  //res.render('index', { title: "Caselles",subtitle: 'Llista de jugadors i puntuacions.' });
});

router.post('/comprobaPartida',function(req,res,next){
  console.log("Petició de comprobar partida rebuda.");
  //console.log(" Body: " + req.body._id);
    //res.send("Falta implementar a index.js linia 18")
    DAOMongo.buscarPartidaUnJugador(res);
});

router.post('/crearPartida',function(req,res,next){
    console.log("Petició de crear partida rebuda");
    DAOMongo.afegirPartida(req.body,res);
});

router.post('/afegirJugador',function(req,res,next){
  console.log("Petició d'afegir jugador rebuda");
  let dades = req.body;
  DAOMongo.afegirJugador(dades,res);
});

router.post('/afegirCasella',function(req,res,next){
  console.log("Petició d'afegir una casella");
  let dades = req.body;
  //console.log(dades.idpartida + "  " + dades.idjugador + "  " + dades.casella);
  DAOMongo.afegirCasella(dades,res);
});

router.post('/afegirTemps',function(req,res,next){
  console.log("Petició d'afegir el temps de la partida");
  let dades = req.body;
  DAOMongo.afegirTemps(dades,res);
});
module.exports = router;
