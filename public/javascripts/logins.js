
var socket;
var ip = 'localhost';


function enviarLogin()
{
    let nom = document.getElementById('tbNom').value;
    let pwd = document.getElementById('tbPwd').value;
    let dades = {"nom":nom,"pwd":pwd};
    socket = io('ws://'+ip+':3000');
    
    //const socket = io();
    socket.on('ResultatValidacio', function(data) {
        console.log("Respuesta del servidor: " + data);
        if(!data)
          alert("No s'ha trobat un jugador amb aquest nom i contrasenya.");
        else
          demanarPartida(data);
      });
      
      socket.on('connect',()=>{
        console.log("Socket del client connectat");
        socket.emit('PeticioValidacio',dades);
      });
    //alert(nom);
}


function demanarPartida(data)
{
   sessionStorage.setItem('dadesjugador',JSON.stringify(data));
   let head = new Headers({"Content-Type":"application/json"});
   let init = {"method":'POST',
               "headers":head,
               "mode":'cors',
               "cache":'default',
               "body": JSON.stringify(data)
               }
   console.log("Init: " + JSON.stringify(data));
   let request = new Request("http://"+ip+":3000/comprobaPartida",init);
   fetch(request).then((response) =>{
       if(response.ok)
       {
           response.text().then((resposta) =>{   
            //alert("Resposta del servidor a enviarResultat: " + (respuesta === '0') );
           if(resposta === "0")
              enviaCrearPartida(data);
           else
              //enviaAfegirJugador(data,resposta)
              enviaAfegirJugador(data,resposta);
         });
       }
       else{
           console.log("Sin respuesta del servidor.");
       }
   });  
}

function enviaCrearPartida(dadesjugador)
{
  let head = new Headers({"Content-Type":"application/json"});
   let init = {"method":'POST',
               "headers":head,
               "mode":'cors',
               "cache":'default',
               "body": JSON.stringify(dadesjugador)
               }
   //console.log("Init: " + JSON.stringify(dadesjugador));
   let request = new Request("http://"+ip+":3000/crearPartida",init);
   fetch(request).then((response)=>{
   if(response.ok)
       {
           response.text().then((resposta) =>{   
            //alert("Resposta del servidor a enviarResultat: " + (respuesta === '0') );
           var parser = new DOMParser();
           var doc = parser.parseFromString(resposta,'text/html');
           win = window.open();
           win.document.write(resposta);
           //win.sessionStorage.setItem("id",resposta._id)
           });
       }
       else{
           console.log("Sin respuesta del servidor.");
       }
      });
}

function enviaAfegirJugador(dadesjugador,dadespartida)
{
  let head = new Headers({"Content-Type":"application/json"});
   let init = {"method":'POST',
               "headers":head,
               "mode":'cors',
               "cache":'default',
               "body": JSON.stringify({"jugador":dadesjugador,"partida":dadespartida})
               }
   //console.log("Init: " + JSON.stringify(dadesjugador));
   let request = new Request("http://"+ip+":3000/afegirJugador",init);
   fetch(request).then((response)=>{
   if(response.ok)
       {
           response.text().then((resposta) =>{   
            //alert("Resposta del servidor a enviarResultat: " + (respuesta === '0') );
           var parser = new DOMParser();
           var doc = parser.parseFromString(resposta,'text/html');
           win = window.open();
           win.document.write(resposta);
           //win.sessionStorage.setItem("id",resposta._id)
           });
       }
       else{
           console.log("Sin respuesta del servidor.");
       }
      });
}



function mostrarPartides()
{    
    let init = {"method":'GET',            
                "mode":'cors',
                "cache":'default',                
                }    
    let request = new Request("http://"+ip+":3000/partides/",init);
    fetch(request).then((response)=>{
    if(response.ok)
        {
            response.text().then((resposta) =>{   
                var parser = new DOMParser();
                var doc = parser.parseFromString(resposta,'text/html');
                win = window.open();
                win.document.write(resposta);
           });
        }
        else{
            console.log("Sin respuesta del servidor.");
        }
       });
 }
