var socket;
var ip = 'localhost';
//var complerta;
var partida;
var dadesjugador = JSON.parse(sessionStorage.getItem('dadesjugador'));
var estat = 0; // Joc no iniciat. 1 iniciat, -1 finalitzat
var segons = 0;


function demanarEstatPartida(id){
    setInterval(()=>socket.emit('PeticioEstatPartida',id),1000);
}

function redibuixaTauler(dadespartida)
{
    let lbestado = document.getElementById('lbestado');
    if(estat >= 0)
    {
        partida = dadespartida;        
        //alert("Partida complerta ?: " + partida.complerta);
        complerta = partida.complerta;
        //console.log("Complerta: " + complerta);
        if(!complerta){
            lbestado.style.background = "yellow";
            lbestado.innerText = "Esperant rival";
        }
        else//Amb partida complerta
        {
            estat = 1;
            segons++;
            let jugador = partida.jugadors.find(player => player._id === dadesjugador._id);
            //console.log("Jugador: " + jugador.nom);
            //console.log("Dades: " + dadesjugador._id);
            lbestado.style.background = "green";
            lbestado.innerText = "Jugador: " + jugador.nom + "\nPartida iniciada !!!: " + jugador.punts ;
        
        //if(!complerta)
            //return;
        let col1 = partida.jugadors[0].color;
        let col2 = partida.jugadors[1].color;
        for(fila in partida.tauler)
            for(columna in partida.tauler[fila])
            {
                let cela = document.getElementById(fila+";"+columna);
                let valor = partida.tauler[fila][columna];
                if(valor === 0)
                {
                cela.style.background = col1;
                }
                else if(valor === 1)
                    cela.style.background = col2;
                
            }
         mostrarTemps();
        }
    }
    else
    {
        lbestado.style.background = "chartreuse";
        lbestado.innerText = "Partida finalitzada";
        desarTemps();
    }
}


/* Pinta la casella polsada del color del jugador li suma un punt */ 
function taulapulsadaClicked(event)
{
    /*
    if(complerta)
        console.log("Partida complerta.");
    alert("Esperant rival");
    */
    let dadesjugador = JSON.parse(sessionStorage.getItem('dadesjugador'));
    //alert(event.target.id);
    //alert(dadesjugador._id);
    //alert(partida._id);
    let dadesenviament = {casella:event.target.id,idjugador:dadesjugador._id,idpartida:partida._id};
    //alert(dadesenviament.casella + "  " + dadesenviament.idjugador);
    //event.target.style.background = "blue";

   let head = new Headers({"Content-Type":"application/json"});
   let init = {"method":'POST',
               "headers":head,
               "mode":'cors',
               "cache":'default',
               "body": JSON.stringify(dadesenviament)
               }
   let request = new Request("http://"+ip+":3000/afegirCasella",init);
   fetch(request).then((response) =>{
       if(response.ok)
       {           
           response.text().then((resposta) =>{   
            //alert("Resposta del servidor a enviarResultat: " + (respuesta === '0') );
             //alert("Resposta del servidor: " + resposta);
             console.log("Resposta del servidor: " + resposta);
         });         
       }
       else{
           console.log("Sin respuesta del servidor.");
       }
   });  
}

function btFinalitzar_clicked()
{
    estat = -1;
}

function mostrarTemps()
{
    let lbtemps = document.getElementById("lbTemps");
    let hores = Math.floor(segons/3600);
    let restm = segons - (hores*3600);
    let minuts = Math.floor(restm/60);
    let rests = restm - (minuts*60);
    lbtemps.innerText = "Temps: " + hores + " h " + minuts + " m " + rests + " s";
}

function desarTemps()
{
    let dadesenviament = {idpartida:partida._id,temps:segons};
   let head = new Headers({"Content-Type":"application/json"});
   let init = {"method":'POST',
               "headers":head,
               "mode":'cors',
               "cache":'default',
               "body": JSON.stringify(dadesenviament)
               }
   let request = new Request("http://"+ip+":3000/afegirTemps",init);
   fetch(request).then((response) =>{
       if(response.ok)
       {           
           response.text().then((resposta) =>{   
            //alert("Resposta del servidor a enviarResultat: " + (respuesta === '0') );
             //alert("Resposta del servidor: " + resposta);
             console.log("Resposta del servidor: " + resposta);
         });         
       }
       else{
           console.log("Sin respuesta del servidor.");
       }
   });  

}