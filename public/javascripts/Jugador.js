class Jugador
{
    constructor(nom,color)
    {
        this.nom = nom;
        this.punts = 0;
        this.color = color;
    }
} //Fi de la classe

module.exports=Jugador;


/* Afegir */
//db.Jugadors.insert({nom:"Jose Antonio",punts:0,color:"green"});

/* Mostrar tots */
/*
 db.Jugadors.find();
{ "_id" : ObjectId("623f579802fffa2dc00c4c70"), "nom" : "Jose Antonio", "punts" : 0, "color" : "green", "pwd" : "Josepwd" }
{ "_id" : ObjectId("623f57b902fffa2dc00c4c71"), "nom" : "Bob Esponja", "punts" : 0, "color" : "yellow", "pwd" : "Bobpwd" }
{ "_id" : ObjectId("623f57d502fffa2dc00c4c72"), "nom" : "Pedro Picapiedra", "punts" : 0, "color" : "brown", "pwd" : "Pedropwd" }
{ "_id" : ObjectId("623f57ff02fffa2dc00c4c73"), "nom" : "Dufy Duck", "punts" : 0, "color" : "blue", "pwd" : "Dufypwd" }
*/

