class Partida{
    constructor(mida)
    {
        this._id = Partida.generaid();
        this.complerta = false;
        this.torn = 1;
        this.jugadors = [];
        this.tauler = [];
        this.temps = 0;
        this.iniciaTauler(mida);
    }
// Partida en mongo:  { "_id" : ObjectId("61d6b81239e03e8b24fe44d3"), "id" : 1, "jugadors" : [ { "nom" : "Jose Antonio", "punts" : 2, "color" : "blue" } ], "tauler" : [ [ -1, -1, -1, -1, -1, -1, -1, -1 ], [ -1, -1, -1, -1, -1, -1, -1, -1 ], [ -1, -1, -1, -1, -1, -1, -1, -1 ], [ -1, -1, -1, -1, -1, -1, -1, -1 ], [ -1, -1, -1, -1, 1, 0, -1, -1 ], [ -1, -1, -1, -1, 0, 1, -1, -1 ], [ -1, -1, -1, -1, -1, -1, -1, -1 ], [ -1, -1, -1, -1, -1, -1, -1, -1 ] ] }

    iniciaTauler(mida)
    {
        for(let i = 0; i < mida; i++)
        {
            this.tauler.push([]);
            for(let j = 0; j < mida; j++)
            {
                this.tauler[i].push(-1);
            }
        }
    }

    static generaid()
    {
        let token1 = Math.random().toString(36).substring(2);
        let token2 = Math.random().toString(36).substring(2);
        return token1+token2;
    }
}

module.exports=Partida;