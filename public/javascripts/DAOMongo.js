var MongoClient = require('mongodb').MongoClient;
var conexioJugadors = 'mongodb://localhost:27017/Jugadors';
var conexioPartides = 'mongodb://localhost:27017/Partides';
let assert = require('assert');
const { resolve } = require('path');
const { nextTick } = require('process');
const Partida = require('./Partida');
const Jugador = require('./Jugador');


class DAOMongo
{
        /* Retorna una array amb tots els jugadors */
         static mostrarJugadors(response)
         {
           //console.log("Cercant partida amb un jugador.")    
           MongoClient.connect(conexioJugadors,function(err,client){
             var db = client.db('Jugadors');            
             db.collection('Jugadors').find()
             .toArray(function(err,result){                               
               client.close();              
               response.render('login', { jugadors:result,title: "Caselles",subtitle: 'Llista de jugadors i puntuacions.' });  
             });
           });
         }

         /* Consulta si existeix un jugador amb el nom i contrasenya passades com argument
            a petició del servidor per mig de web sockets  */
         static async validarJugador(nom,pwd,socket)
         {
            MongoClient.connect(conexioJugadors,function(err,client){
            var db = client.db('Jugadors'); 
            // Obtenir jugador amb nom i password 
                db.collection('Jugadors').findOne({nom:nom,pwd:pwd},function(err,result){
                  socket.emit('ResultatValidacio', result);                              
                })              
            });          
         }

         static buscarPartidaUnJugador(response)
         {
          response.writeHead(200, {"Content-Type": "application/json"});   
          //console.log("Cercant partida amb un jugador.")    
          MongoClient.connect(conexioPartides,function(err,client){
            var db = client.db('Partides');            
            db.collection('Partides').aggregate([{$project:{count:{$size:"$jugadors"}}}])
            .toArray(function(err,result){                               
                let i,trobada = false;           
                for(i = 0; i < result.length;i++)
                {
                  if(result[i].count === 1)
                    {
                      trobada = true;
                      break;
                    }
                }                
                if(trobada)
                  response.write(JSON.stringify(result[i]));
                else
                  response.write(JSON.stringify(0));                                      
              client.close();              
              response.end();    
            });
          });
         }

         static afegirPartida(jugador,res)
         {
           let mida = DAOMongo.obtenirTamany(jugador.punts);
           let novapartida = new Partida(mida);
           jugador.punts = 0;
           novapartida.jugadors.push(jugador);
           console.log("Jugador en partida: " + novapartida.jugadors[0]);
           MongoClient.connect(conexioPartides,function(err,client){
            assert.equal(null, err);
            console.log("Connexió correcta");
            var db = client.db('Partides');
            db.collection('Partides')
            .insertOne({"_id":novapartida._id, "complerta":novapartida.complerta, "torn":novapartida.torn,
                        "jugadors":novapartida.jugadors,"tauler":novapartida.tauler,"temps":novapartida.temps});
            assert.equal(err, null);
            console.log("Afegit document a col·lecció Partides");  
            res.render('mostraPartida',{partida:novapartida});   
            res.end();                  
            });           
         }

         static obtenirTamany(punts)
         {
            if(punts === 0)
                return 10;
            else if(punts < 50)
                return 15;
            else
                return 20;     
         }

         static enviarPartida(id,socket)
         {
          MongoClient.connect(conexioPartides,function(err,client){
            var db = client.db('Partides'); 
            // Obtenir la partida amb un jugador 
                db.collection('Partides').findOne({_id:id},function(err,result){
                  //console.log("Resultado: " + result._id );   
                  //console.log("Partida: " + result);
                  socket.emit("EstatPartida",result); 
                  client.close(); 
                })          
           });
         }

         static afegirJugador(dades,res)
         {          
          let idpartida = JSON.parse(dades.partida)._id;
          let j = dades.jugador;
          dades.jugador.punts = 0;
          //console.log("En Mongo: " + idpartida + "  " + j);
          
          MongoClient.connect(conexioPartides,function(err,client){
            var db = client.db('Partides'); 
            //Obtenir la partida amb un jugador 
            let p = new Promise((resolver,reject)=>{
                db.collection('Partides').findOne({_id:idpartida},function(err,result){                
                  resolver(result);                
                })          
            });
            // Afegir el nou jugador a la partida trobada y posar complerta a true
            p.then((partida) => { 
               db.collection('Partides').updateOne({ _id: partida._id},{$set: {"jugadors.1": j,"complerta":true}});
               res.render('mostraPartida',{partida:partida});   
               res.end();                  
              });
          });
          
         }

         static afegirCasella(dades,res)
         {
           //**Obtener la array de jugadores de la partida con idpartida --OK--
           //**Bucar idjugador en la lista de jugadores de la partida y obtener su indice --OK--
           //**Si la casilla del tablero tiene -1, escribir la casilla del tablero con un 0 o un 1
           //    segun indice del jugador y añadir un punto al jugador.
           let idpartida = dades.idpartida;
           let idjugador = dades.idjugador;
           let casella = dades.casella;
           
           // Obtener la array de jugadores de la partida con idpartida
           MongoClient.connect(conexioPartides,async function(err,client){
            var db = client.db('Partides'); 
            // Obtenir la array de jugadors de la partida
            let p = new Promise((resolver,reject)=>{
                db.collection('Partides').findOne({_id:idpartida},{jugadors:1},function(err,result){                
                  resolver(result);                
                })          
            });
            // Afegir la fitxa a la casella de partida trobada en els index indicats
            p.then((partida) => { 
              /*
               let r =db.collection('partides')
               .updateOne({ _id: partida._id},{$set: {["tauler."+fila+"."+columna]: color*1}});             
               response.end();
               */
                //Obtenir index del jugador a l'array de jugadors de la partida                
                let primer = partida.jugadors[0]._id === idjugador;
                let index = 1;
                if(primer === true)
                  index = 0;
                //Comprobar si la casella esta buida
                let tauler = partida.tauler;
                let sep = casella.indexOf(';');
                let fila = casella.substring(0,sep);
                let columna = casella.substring(sep+1);
                console.log("Tauler: " + tauler + " Fila: " + fila + "  Columna: " + columna);
                if(isNaN(fila) || isNaN(columna) || fila === undefined || columna === undefined || 
                    tauler === undefined || fila.length === 0 || columna.length === 0 )
                {
                  res.write("No actualitzat");
                  res.end();
                  return;
                }
                let buida = (tauler[fila][columna])*1 < 0;
                let punts = partida.jugadors[index].punts;
                punts++;
                //console.log("Fila: " + fila + "Columna: " + columna);
                //console.log("Files: " + tauler.length + " Columnes: " + tauler[0].length);
                //console.log("Buida: " + buida + " Valor: " + (tauler[fila][columna])*1);
                if(!buida)
                {
                  res.write("no buida");
                  res.end();
                  return;
                }
                console.log("Actualizando");
                 //Escribir la casilla del tablero con un 0 o un 1 segun indice del jugador
                // y añadir un punto al jugador.
                let p = new Promise((resolver,reject)=>{                
                  let r =db.collection('Partides')
                    .updateOne({ _id:partida._id},{$set: {["tauler."+fila+"."+columna]:index,["jugadors."+index+".punts"]:punts}});
                    resolver(r); 
                  // Afegir la fitxa a la casella de partida trobada en els index indicats    
                  }).then((resultat) => {
                    console.log("Resultat: " + resultat.modifiedCount + " Punts: " + punts);
                    res.write("actualitzat");
                    res.end();
                  });                                                                                                
              });
           });
        
         }

         static afegirTemps(dades,res)
         {
           //**Obtenir la partida amb l'id de les dades
           //**Si el temps no es zero, no fer res, ja s'ha desat
           //**Escriure el temps a la partida
           let idpartida = dades.idpartida;           
           let tempsp = dades.temps;           
           // Obtenir la partida
           MongoClient.connect(conexioPartides,async function(err,client){
            var db = client.db('Partides'); 
            // Obtenir la partida
            let p = new Promise((resolver,reject)=>{
                db.collection('Partides').findOne({_id:idpartida},function(err,result){                
                  resolver(result);                
                })          
            });
            // Afegir la fitxa a la casella de partida trobada en els index indicats
            p.then((partida) => { 
                if(partida.temps > 0)
                {
                  res.end();
                  return;
                }            
                // afegir el temps a la partida.
                let p = new Promise((resolver,reject)=>{                
                  let r =db.collection('Partides')
                    .updateOne({ _id:partida._id},{$set: {temps:tempsp}});
                    resolver(r); 
                  // Afegir la fitxa a la casella de partida trobada en els index indicats    
                  }).then((resultat) => {
                    //Afegir els punts als jugadors
                    //console.log("Jugador1: " + Object.keys(partida.jugadors[0]) + " Jugador2: " + partida.jugadors[1]._id);
                    //console.log("Resultat: " + resultat.modifiedCount + " Temps: " + tempsp);
                    MongoClient.connect(conexioJugadors,function(err,cliente){
                      let p = new Promise((resolver,reject)=>{                
                        let db2 = cliente.db('Jugadors');                                            
                        console.log("Punts del jugador: " + tempsp);
                        console.log("Id del jugador: " + partida.jugadors[0]._id );
                        let r = db2.collection('Jugadors')                           
                        .updateOne({_id:partida.jugadors[0]._id},{$set:{punts:partida.jugadors[0].punts}});                                                                      
                        //.updateOne({_id:partida.jugadors[0]._id},{$set:{punts:0}});                                                                      
                        resolver(r);
                      })                      
                      .then((result)=>{
                        console.log("Resultat: " + result.modifiedCount);
                        let p = new Promise((resolver,reject)=>{   
                         let db2 = cliente.db('Jugadors');                                      
                         db2.collection('Jugadors')
                          .updateOne({_id:partida.jugadors[1]._id},{$set:{punts:partida.jugadors[1].punts}});                     
                          //.updateOne({_id:partida.jugadors[1]._id},{$set:{punts:0}});                     
                      });
                    })//Primer then tras update                    
                    .then(()=>{
                        res.write("actualitzat");
                        res.end();
                      });                                                                                          
                    });//connect                                 
                  });                                                            
              });
           });
         }

         static mostrarPartides(res)
         {           
          MongoClient.connect(conexioPartides,function(err,client){
            var db = client.db('Partides');            
            db.collection('Partides').find()
            .toArray(function(err,result){                               
              client.close();              
           res.render('partides',{partides:result,title:'Caelles',subtitle:'Partides anteriors'});
            });
          });
        }

}//Fi de la classe





module.exports=DAOMongo;